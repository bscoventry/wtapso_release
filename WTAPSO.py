"""
Created on Mon Nov 4 09:40 2019

@author: Brandon S Coventry. Weldon School of Biomedical  Purdue University
Purpose: This is the control function for calling WTAPSO optimization
Included files: AgentClass - Holds the class functions for WTAPSO
                testOptimizationFunctions - Mathematical definitions of test functions used to test WTAPSO 
                WTAPSO_HelperFunctions - File holding helper functions to assist in swarm creation and running
Revision History: 05/12/20 GPU support added for calculations in numpy via cuPy module. 
Notes: Reference: Coventry et al., 2017 J. Comput Neurosci
"""
import numpy as np                 #GPU availability. Replace cupy with numpy for CPU support.
import pdb
from AgentClass import Particle
from WTAPSO_HelperFunctions import genSwarmNetwork,updateSwarmNetwork,updateNeighborErrors,updateSwarm    #Import all the swarm helper functions
class WTAPSO(object):
    """ This is the Winner-Take-All Particle Swarm optimization class. Takes in the cost function on input for optimization."""
    def __init__(self,problemDimension,costfunc,numIter=1000,softConstraints=[np.inf,np.inf],randStartVals=[],phi1 = 4.05,phi2 = 4.05,chi = 0.7298):
        """Inputs:
            problemDimension: Number of parameters being iterated over. Future updates will read this directly.
            costfunc: Variable referencing the cost function. Can feed a function directly into this variable.
            numIter: In this version, optimization stops after a number of iterations. A future update will do costfunction change approaches.
            softConstraints: With evolutionary algorithms, soft constraints can be used. Position 0 is lower bound, position 1 upper bound. Set to np.inf if no constraints needed.
            randStartVals: Variable can be randomly or deterministically set as needed. This is the start condition for the algorithm. Recommend stochastic initialization for most cases.
            phi1,phi2 are search parameters for PSO. Suggest to keep at 4.05 unless playing with searching speeds. May lead to adverse behaviour if changed however.
            chi: The PSO dampening parameter. Suggest to keep at 0.7298 to prevent oscillatory behaviour.
            """
        super(WTAPSO, self).__init__()
        #Set instance variables here.
        self.problemDimension = problemDimension
        self.costfunc = costfunc
        self.numIter = numIter
        self.softConstraints = softConstraints
        self.randStartVals = randStartVals
        self.phi1 = phi1
        self.phi2 = phi2
        self.chi = chi
        self.swarmSize = 21                                          #For the moment, keep this at 21 for canonical WTAPSO. Updates will allow for arbitrarily large swarm sizes
        self.errorvec = np.ones(self.swarmSize)*np.inf               #Often helpful to keep track of fitness in an outside variable   
        self.minerr = 0                                              #This holds the min error value of the best (leader) agent.
        self.bestagent = []                                          #Holds the best agent, which is by definition, the swarm leader.
        self.nummem = 5                                              #The number of agents in a neighborhood swarm. Keep at 5 for the moment.
        self.neisize = np.floor(self.swarmSize/self.nummem)          #Defines the size of the neighborhoods here
        self.matrixdim = [self.nummem,4]                             #Size of matrices storing neighborhood info.     
        self.N = []                                                  #Contains information the location of each agent (except best agent) in the swarm. Rows are different neighborhoods
        self.errind = []                                             #Internal variable for holding locations of agents with given error values
        self.sizes = []
        self.neibest = np.inf                                        #Internal variable for holding a given neighborhood leader
        self.agenterrorbest = np.inf                                 #Internal variable for control of swarm updates
        self.agenterrbest = np.inf                                   #Internal variable for control of swarm updates
        self.row = 0                                                 #Internal Variable for control of swarm updates
        self.col = 0
    def createSwarm(self):
        """This function will create the swarm and assign control parameters to each agent"""
        self.swarm = [Particle(np.random.uniform(self.randStartVals[0],self.randStartVals[1],[self.problemDimension]),np.zeros(self.problemDimension),self.chi,self.phi1,self.phi2,np.inf) for ck in range(self.swarmSize)]   #Generate the swarm!!!
        for bc in range(self.swarmSize):                     #Set the agent phi and chi values
            self.swarm[bc].setChi(self.chi)
            self.swarm[bc].setPhi1(self.phi1)
            self.swarm[bc].setPhi2(self.phi2)

    def runSwarmInit(self):
        """This function is for the first run of the swarm. Sets fitness and position values"""
        for ii in range(self.swarmSize):
            curagent = self.swarm[ii]                        #Grab agent in the swarm
            curinputvals = curagent.returnPosition()      #Grab the inputs to the cost function
            curfitness = self.costfunc(curinputvals)         #Calculate a new fitness
            if curfitness < self.errorvec[ii]:
                self.swarm[ii].setFitness(curfitness)            #Set fitness for each agent
                self.swarm[ii].setBestPosition(curagent.returnPosition())       #At the first run, best position is the current position
                self.errorvec[ii] = self.swarm[ii].returnFitness()
    
    def createSwarmNet(self):
        """Function to create the swarm network and generate first best errors"""
        self.minerr = np.argmin(self.errorvec)                    #Grab the position of the best performing agent.
        self.bestagent = self.swarm[self.minerr] 
        self.genSwarmNetwork()     #Generate swarm matrix
        self.row = self.sizes[0]
        self.col = self.sizes[1]
        self.neibest = np.zeros(int(self.neisize))                     #Vector holding the agent leader values.
        self.updateNeighborErrors()

    def genSwarmNetwork(self):
        k = 0                             #Variable k is used in modulo to create network
        self.N = np.zeros([self.matrixdim[0],self.matrixdim[1]])        #N Holds our network
        for jj in range(self.swarmSize):           #Iterate through the agents and place them in the network
            if k <= self.nummem-1:
                self.N[k,int(np.mod(jj,self.neisize))] = jj
            else:
                k = 0                         #Reset mod factor
                self.N[k,int(np.mod(jj,self.neisize))] = jj
            k = k + 1
        #If agent 21 isn't a leader, then all agents are placed except 21. We swap it in for the global leader
        self.errind = np.where(self.N==self.minerr) #Find where best agent is in the matrix
        if self.minerr != self.swarmSize-1:
            self.N[self.errind[0],self.errind[1]] = self.swarmSize
        self.sizes = np.shape(self.N)
        #return [N,sizes,errind]
    def updateNeighborErrors(self):
        """ This function is used to update the N matrix (matrix containing neighborhood agent information) in the update step
    of the WTAPSO. To be used after the first run of the swarm and all consequent update steps. """
    #errind = np.where(N == minerr)
    #if minerr != swarmsize-1:
        #N[errind[0],errind[1]] = swarmsize
    #sizes = N.shape
    #N[errind[0],errind[1]] = swarmsize             # Replace the best (it will have its own holder) with agent 21
    #sizes = N.shape
    #row = sizes[0]                              #Get the matrix dimensions
    #col = sizes[1]
    #neibest = np.zeros(int(neisize))
        for kk in range(self.col):                     #Iterate through neighborhoods, which are columns of N
            agentvals = self.N[:,kk]
            errv = np.zeros(len(agentvals))
            for ll in range(len(agentvals)):
                curagent = self.swarm[int(agentvals[ll]-1)]
                errv[ll] = curagent.returnFitness()
            minern = np.where(errv==np.min(errv))
            self.neibest[kk] = agentvals[minern[0][0]]
        #minerr = np.argmin(errorvec)
        self.agenterrorbest = self.swarm[self.minerr]
        self.agenterrbest = self.agenterrorbest.returnFitness
        # [self.agenterrorbest,self.agenterrbest,self.neibest]
    
    def optimize(self):
        for ck in range(self.numIter):
            self.updateSwarm()         #Run the swarm update to update each agent
            #swarm = swarm[0]
            for bc in range(self.swarmSize):
                curagent = self.swarm[bc]                        #Grab agent in the swarm
                curinputvals = curagent.returnPosition()      #Grab the inputs to the cost function
                curfitness = self.costfunc(curinputvals)         #Calculate a new fitness
                if curfitness < self.errorvec[bc]:
                    self.swarm[bc].setFitness(curfitness)            #Set fitness for each agent
                    self.swarm[bc].setBestPosition(curagent.returnPosition())       #At the first run, best position is the current position
                    self.errorvec[bc] = self.swarm[bc].returnFitness()
                if bc == self.minerr:
                    print(self.errorvec[self.minerr])
            self.updateSwarmNetwork()
        #return [self.bestagent,self.errorvec,self.swarm]
    def updateSwarm(self):
        """ This function updates the swarm. Loops through and finds global and local leaders
        and reorganizes the swarm based on these values."""
        swarmup = self.swarm                     #Create an identical copy for update.
        neibesterrors = self.errorvec[self.neibest.astype(int)-1]       #Grab the neighborhood leader values
        neibesterr = np.min(neibesterrors)
        if np.size(neibesterr)>1:
            neibesterr = neibesterr[1]           #Check for duplicates
        errneibestindex = np.where(self.errorvec==neibesterr)
        bestneileader = self.swarm[errneibestindex[0][0]]
        self.bestagent.updateAgent(self.bestagent.returnBestPosition(),bestneileader.returnBestPosition(),self.softConstraints)  #Class function to update. See update description for how best, neibest, and regular agents are updated
        swarmup[self.minerr] = self.bestagent
        """Now update neighborhood leaders"""
        for nn in range(self.col):
            updateneibest = self.neibest[nn]
            update = swarmup[int(updateneibest)-1]         #Grab the neighborhood leader for each subswarm
            neighbor = self.N[:,nn]
            neierrvec = self.errorvec[neighbor.astype(int)-1]        #Vector containing errors of local leader agents
            neighbesterr = np.min(neierrvec)      #Now grab the error of the best neighborhood agent, use to grab best
            if np.size(neighbesterr)>1:
                neighbesterr = neibesterr[0]      #If multiple have same error value, just choose 1
            bestneigh = np.where(self.errorvec==neighbesterr)           #Find the location of the best neighbor in the swam
            updateneigh = self.swarm[bestneigh[0][0]-1]       #Now that we have found the best neighbor, we grab it from the swarm
            update.updateAgent(self.bestagent.returnBestPosition(),updateneigh.returnBestPosition(),self.softConstraints)
            swarmup[int(updateneibest)-1]=update
        """Now update the Neighborhood values"""
        for mm in range(self.col):               #We again poll the neighborhood
            currentneigh = self.N[:,mm]
            currentneiwhere = np.where(currentneigh!=self.neibest[mm])             #Search through the local neighborhood, ignoring the leader, we've already updated them
            currentneigh = currentneigh[currentneiwhere]            
            upneibest = self.swarm[int(self.neibest[mm])-1]       ##Since this agent is defined as a neighborhood leader, it will default be a part of the individual neighbor update
            for ii in range(len(currentneigh)):           #Update individual neighbors
                upvar = self.swarm[int(currentneigh[ii])-1]        #Grab the agent from the swarm
                neierrvecindwhere = np.where(currentneigh!=currentneigh[ii])
                neierrvecind = currentneigh[neierrvecindwhere]
                neierrvec = self.errorvec[(neierrvecind.astype(int))-1]        #Grab agent who is performing best
                neierrvec = np.min(neierrvec)
                if np.size(neierrvec) > 1:
                    neierrvec = neierrvec[0]              #If multiple occur, just choose one.
                bnei = np.where(self.errorvec==neierrvec)
                upN = self.swarm[bnei[0][0]]            #Pull agent from swarm
                upvar.updateAgent(upneibest.returnBestPosition(),upN.returnBestPosition(),self.softConstraints)
                swarmup[int(currentneigh[ii])-1] = upvar
        #return [swarmup,minerr]
        self.swarm = swarmup
    def updateSwarmNetwork(self):
        k = 0                      #Starting point for neighborhood generation
        self.neisize = int(self.neisize)
        for jj in range(self.swarmSize):
            if k <= self.nummem-1:
                self.N[k,np.mod(jj,self.neisize)]= jj
            else:
                k = 0
                self.N[k,np.mod(jj,self.neisize)] = jj
            k = k+1
        #If agent 21 isn't a leader, then all agents are placed except 21. We need to swap 21 for global leader, which sits outside this network structure
        errind = np.where(self.N==self.minerr)               #find where best agent is in the network
        self.N[errind[0],errind[1]] = self.swarmSize         #Replace the best (it will have its own holder) with agent 21 and reorganize
        self.sizes = np.shape(self.N)                  #Get matrix dimensions
        self.row = self.sizes[0]
        self.col = self.sizes[1]       
        self.neibest = np.zeros(self.neisize)
        for kk in range(self.col):
            curcol = kk
            agentvals = self.N[:,kk]
            errv = np.zeros(len(agentvals))
            for rr in range(len(agentvals)):
                curagent = self.swarm[int(agentvals[rr])-1]
                errv[rr] = curagent.returnFitness()
            minern = np.where(errv==np.min(errv))
            self.agenterrorbest = self.swarm[self.minerr]
            self.agenterrbest = self.agenterrorbest.returnFitness
        #return [N,agenterrorbest,agenterrbest]
    def runOptimization(self):
        """This function runs the full optimization. Call this after declaring class"""
        self.createSwarm()
        self.runSwarmInit()
        self.createSwarmNet()
        self.optimize()
        return self.bestagent.returnBestPosition(),self.bestagent.returnFitness()