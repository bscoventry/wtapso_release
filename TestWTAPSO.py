"""
Test WTAPSO
"""
from WTAPSO import *
from testOptimizationFunctions import *
costFunc = Rastrigin
problemDimension = 2             #Dimensionality of problem under optimization
softConstraintsLow = np.inf*np.ones([1,problemDimension])               #Option for soft constraint of features. Set to inf if it is to be ignored.
softConstraintsHigh = np.inf*np.ones([1,problemDimension])
softConstraints = [softConstraintsLow,softConstraintsHigh]
randstart = [-5.12,5.12] 
phi1 = 2.05                             #Phi1,phi2, and chi control swarming behaviour, trading off between exploration and seeking trends towards good results
phi2 = 2.05
chi =  0.7298
numIter = 1000   
optClass = WTAPSO(problemDimension, costFunc, numIter, softConstraints, randstart, phi1,phi2,chi)
bestWeights,CostFinal = optClass.runOptimization()
print(bestWeights)
print(CostFinal)