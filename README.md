# WTAPSO_Release

Reference: Coventry BS, Parthasarathy A, Sommer AL, Bartlett EL. (2017) "Hierarchical winner-take-all particle swarm optimization social network for neural model fitting." Journal of Computational Neuroscience 42(1):71-85.

## Winner Take All Particle Swarm Optimization
This package implements the winner take all varient of particle swarm optimization. This social network mimics heiracrhical winner take all coding found in primary visual cortex. Particle swarm agents
and their relevant class functions are implemented in the "agent.py" class described below. Agents are organized into heirarchical social networks which compete for influence over the swarm at large. The
network consists of 4 neighborhoods consisting of 5 agents with one designated as a neighborhood leader. The four neighborhood leaders then influence and draw influence from their constituient neighborhood agents and global network leader, who represents
the best performing agent overall. Neighborhood agents draw influenece and are influenced by their neighbors and their neighboorhood leaders. As the particles swarm, if any individual agent finds a path towards better
maxima/minima, they are allowed to usurp their neighborhood leader, or even make the jump to global leader if drastically better solutions are found. 

Version: 2.1

## Running WTAPSO
To run the optimization algorithm, one only needs to pass a small number of arguments into the class initializer.

**newWTAPSOClass = WTAPSO(problemDimension, costFunction, numIterations, softConstraints, randstart, phi1, phi2, chi)**

with

problemDimension being an int defining the total number of dimensions or weights of the cost function

costFunction is a variable for passing in a cost function definition

(Optional) numIterations is the number of iterations till the optimization algorithm completes (If not specified, defaults to 1000)

(Optional) softConstraints can be used to add soft constraints to the cost function parameters (If not specified, defaults to low = infinitiy, high = infinity or no constraints)

(Optional) randstart specifies the start values of the parameters, which are defined outside the class by a users choice of random variable (if not specified, defaults to null)

(Optional) phi1, phi2 specifies the phi parameters in control of the swarming motion of the particle swarm algorithm. (If not specified, phi1=phi2=4.05)

(Optional) chi specifies the chi parameter in speed of swarm movement in optimization. (If not specified, chi = 0.7298)

After initial class definition, the swarm will complete with:

**optParams,finCost = newWTAPSOClass.runOptimization() **

with optParams returning the parameters of the best performing agent with cost finCost. 